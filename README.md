# Tweetron LS (Tweetron 延命版)

TweetronはOBSブラウザーソースを使用してツイートを画面上に表示するツールソフトです

Windowsのみ対応 (Windows10動作確認済)

## 延命措置について

この派生版は公式 API ではなく、旧 TweetDeck が使用する API を利用しています。
そのため、TweetDeck の有料化に伴い使えなくなる可能性が大きいです。

[本家](https://github.com/Tweetron/)

## ダウンロード
[v0.5.3-ls1](https://gitlab.com/alubaato/tweetron-ls/uploads/17c33d650bc14471c07579ef08140d75/Tweetron_v0.5.3-ls1.zip)


## 使用方法

使用方法については以下のwikiを御覧ください

[https://github.com/Tweetron/Tweetron/wiki](https://github.com/Tweetron/Tweetron/wiki)


## License

[MIT License](https://github.com/Tweetron/Tweetron/blob/main/LICENSE)
