# Some shitty code to keep ths app running
import time
import requests
from easydict import EasyDict

# see https://tinyurl.com/2p87dehk
ACCESS_TOKEN  = "AAAAAAAAAAAAAAAAAAAAAFQODgEAAAAAVHTp76lzh3rFzcHbmHVvQxYYpTw%3DckAlMINMjmCwxUcaXbAN4XqJVdgMJaHqNOFgPMK0zN1qLqLQCF"

session = requests.Session()
session.headers.update({
    "User-Agent": None,
    "Authorization": f"Bearer {ACCESS_TOKEN}",
    "connection": "keep-alive",
    "content-type": "application/json",
    "x-twitter-active-user": "yes",
    "authority": "api.twitter.com",
    "accept-encoding": "gzip",
    "accept-language": "en-US,en;q=0.9",
    "accept": "*/*",
    "DNT": "1"
})


def update_guest_token():
    response = session.post("https://api.twitter.com/1.1/guest/activate.json")
    session.headers["x-guest-token"] =  response.json()['guest_token']

update_guest_token()

def search_tweets(query):
    print(f"search_tweets: {query}")

    retries = 5
    response = None

    while retries > 0:
        try:
            response = session.get(
                "https://api.twitter.com/1.1/search/universal.json",
                params={
                    "q": query,
                    "modules": "status",
                    "result_type": "recent"
                }
            )

            if response.ok:
                break

        except:
            retries -= 1
            update_guest_token()
            time.sleep(1)

    else:
        print("Failed to search tweets!")
        return []

    return [module.status.data for module in EasyDict(response.json()).modules]
